/*
 * quantity.h
 *
 *  Created on: 2018/12/22
 *      Author: Takuya Makimoto
 */

#ifndef QUANTITY_H_
#define QUANTITY_H_

#include <algorithm>

namespace cfd {

/**
 *
 */
template<typename T>
class quantity {
public:
	enum {
		Dencity, Momentum, Energy, Size
	};

public:
	quantity() :
			x_() {
	}

	quantity(const quantity& other) :
			x_() {
		std::copy(other.x_, other.x_ + quantity::Size, this->x_);
	}

	quantity(const T& rho, const T& u, const T& e) :
			x_ { rho, u, e } {

	}

	~quantity() {
	}

	T operator[](std::size_t n) const {
		return this->x_[n];
	}

	T& operator[](std::size_t n) {
		return this->x_[n];
	}

	quantity& operator=(const quantity& rhs) {
		if (&rhs == this) {
			return *this;
		}

		std::copy(rhs.x_, rhs.x_ + quantity::Size, this->x_);
		return *this;
	}

	quantity& operator+=(const quantity& rhs) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] += rhs.x_[i];
		}
		return *this;
	}

	quantity& operator-=(const quantity& rhs) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] -= rhs.x_[i];
		}
		return *this;
	}

	quantity& operator*=(float alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] *= alpha;
		}
		return *this;
	}

	quantity& operator*=(double alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] *= alpha;
		}
		return *this;
	}

	quantity& operator*=(long double alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] *= alpha;
		}
		return *this;
	}

	quantity& operator/=(float alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] /= alpha;
		}
		return *this;
	}

	quantity& operator/=(double alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] /= alpha;
		}
		return *this;
	}

	quantity& operator/=(long double alpha) {
		for (std::size_t i = 0; i < quantity::Size; i++) {
			this->x_[i] *= alpha;
		}
		return *this;
	}
private:
	T x_[Size];
};

template<typename T>
quantity<T> operator+(const quantity<T>& x, const quantity<T>& y) {
	quantity<T> z(x);
	z += y;
	return z;
}

template<typename T>
quantity<T> operator-(const quantity<T>& x, const quantity<T>& y) {
	quantity<T> z(x);
	z -= y;
	return z;
}

template<typename T>
quantity<T> operator*(const quantity<T>& x, float alpha) {
	quantity<T> y(x);
	y *= alpha;
	return y;
}

template<typename T>
quantity<T> operator*(const quantity<T>& x, double alpha) {
	quantity<T> y(x);
	y *= alpha;
	return y;
}

template<typename T>
quantity<T> operator*(const quantity<T>& x, long double alpha) {
	quantity<T> y(x);
	y *= alpha;
	return y;
}

template<typename T>
quantity<T> operator*(float alpha, const quantity<T>& x) {
	return operator*(x, alpha);
}

template<typename T>
quantity<T> operator*(double alpha, const quantity<T>& x) {
	return operator*(x, alpha);
}

template<typename T>
quantity<T> operator*(long double alpha, const quantity<T>& x) {
	return operator*(x, alpha);
}

template<typename T>
quantity<T> operator/(const quantity<T>& x, float alpha) {
	quantity<T> y(x);
	y /= alpha;
	return y;
}

template<typename T>
quantity<T> operator/(const quantity<T>& x, double alpha) {
	quantity<T> y(x);
	y /= alpha;
	return y;
}

template<typename T>
quantity<T> operator/(const quantity<T>& x, long double alpha) {
	quantity<T> y(x);
	y /= alpha;
	return y;
}

} /* namespace cfd */

#endif /* QUANTITY_H_ */

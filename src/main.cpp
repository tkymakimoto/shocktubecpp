/*
 * main.cpp
 *
 *  Created on: 2018/05/07
 *      Author: tmakimoto
 */

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>

#include <Eigen/Core>
#include <Eigen/Eigenvalues>

#include "quantity.h"

/*
 * 定数
 */
static const double Gamma = 1.4;
//	const double R = 287.0;	// [JK/kg]

Eigen::Matrix3Xd muscl_with_van_albada(const Eigen::Matrix3Xd &Q);
Eigen::Matrix3Xd muscl(const Eigen::Matrix3Xd &Q);
Eigen::Matrix3Xd roe(const Eigen::Matrix3Xd &Q);

void euler_time_integral(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F);

void runge_kutta_2nd_roe(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F);

void runge_kutta_4th_roe(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F);

void runge_kutta_2nd_muscl(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F);

int main(int argc, char **argv) {

	typedef cfd::quantity<double> quantity_d;

	/*
	 * 時間
	 */
	const double tmin = 0.0;
	const double tmax = 0.3;
	const double CFL = 1.0;

	/*
	 * 格子設定
	 */
	const double Xmin = -0.5;
	const double Xmax = 0.5;
	const Eigen::Index N = 100;
	const double dx = (Xmax - Xmin) / N;
	double x[N] = { 0.0 };
	for (size_t i = 0; i < N; i++) {
		x[i] = Xmin + 0.5 * dx + i * dx;
	}

	/*
	 * 初期条件
	 */
	const double p_L = 1.0;
	const double rho_L = 1.0;
	Eigen::Vector3d q_L(rho_L, 0.0, p_L / (Gamma - 1.0));

	const double p_R = 0.1;
	const double rho_R = 0.125;
	Eigen::Vector3d q_R(rho_R, 0.0, p_R / (Gamma - 1.0));

	Eigen::Matrix3Xd Q = Eigen::Matrix3Xd::Zero(quantity_d::Size, N + 2);
	for (size_t i = 0; i < N / 2 + 1; i++) {
		Q.col(i) = q_L;
	}
	for (int i = N / 2 + 1; i < Q.cols(); i++) {
		Q.col(i) = q_R;
	}

	double time = tmin;
	{
		std::ostringstream oss;
		oss << std::scientific << std::setprecision(3) << time;
		std::string filename = "data_" + oss.str() + ".out";
		std::ofstream ofs(filename);

		for (Eigen::Index i = 1; i < N + 1; i++) {
			const double rho = Q.col(i)[quantity_d::Dencity];
			const double u = Q.col(i)[quantity_d::Momentum]
					/ Q.col(i)[quantity_d::Dencity];
			const double p = (Gamma - 1.0)
					* (Q.col(i)[quantity_d::Energy] - 0.5 * rho * u * u);

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< x[i - 1];

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< rho;

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< u;

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< p;

			ofs << std::endl;
		}

		ofs.close();
	}
	do {
#ifdef DEBUG
		std::cout << "time = " << time << "[sec]" << std::endl;
#endif

		Eigen::Matrix3Xd FFbnd = roe(Q); //roe(Q);

		/*
		 * 時間積分
		 */
		double u_max = 0.0;
		for (Eigen::Index i = 1; i < N + 1; i++) {
			const double rho = Q.col(i)[quantity_d::Dencity];
			const double u = Q.col(i)[quantity_d::Momentum] / rho;
			const double e = Q.col(i)[quantity_d::Energy];
			const double p = (Gamma - 1.0) * (e - 0.5 * rho * u * u);
			u_max = std::max(u_max, u + std::sqrt(Gamma * p / rho));
		}

		const double dt = 1.0e-3;
//		const double dt = CFL * dx / u_max;
//		euler_time_integral(dt, dx, Q, FFbnd);
//		runge_kutta_2nd_roe(dt, dx, Q, FFbnd);
		runge_kutta_4th_roe(dt, dx, Q, FFbnd);
//		runge_kutta_2nd_muscl(dt, dx, Q, FFbnd);

		time += dt;

		std::ostringstream oss;
		oss << std::scientific << std::setprecision(3) << time;
		std::string filename = "data_" + oss.str() + ".out";
		std::ofstream ofs(filename);

		for (Eigen::Index i = 1; i < N + 1; i++) {
			const double rho = Q.col(i)[quantity_d::Dencity];
			const double u = Q.col(i)[quantity_d::Momentum]
					/ Q.col(i)[quantity_d::Dencity];
			const double p = (Gamma - 1.0)
					* (Q.col(i)[quantity_d::Energy] - 0.5 * rho * u * u);

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< x[i - 1];

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< rho;

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< u;

			ofs << std::scientific << std::setprecision(5) << std::setw(13)
					<< p;

			ofs << std::endl;
		}

		ofs.close();

	} while (time < tmax);

	exit(EXIT_SUCCESS);
}

Eigen::Matrix3Xd muscl_with_van_albada(const Eigen::Matrix3Xd &Q) {

	typedef cfd::quantity<double> quantity_d;

	/*
	 * 流束Flux
	 */
	Eigen::Matrix3Xd FF = Eigen::Matrix3Xd::Zero(quantity_d::Size, Q.cols());
	for (Eigen::Index i = 0; i < Q.cols(); i++) {
		const double rho = Q.col(i)[quantity_d::Dencity];
		const double u = Q.col(i)[quantity_d::Momentum]
				/ Q.col(i)[quantity_d::Dencity];
		const double e = Q.col(i)[quantity_d::Energy];
		const double p = (Gamma - 1.0) * (e - 0.5 * rho * u * u);

		FF.col(i)[quantity_d::Dencity] = Q.col(i)[quantity_d::Momentum];
		FF.col(i)[quantity_d::Momentum] = p + rho * u * u;
		FF.col(i)[quantity_d::Energy] = (e + p) * u;
	}

	// セル境界流束
	Eigen::Matrix3Xd FFbnd = Eigen::Matrix3Xd::Zero(quantity_d::Size,
			Q.cols() - 1);
	for (Eigen::Index i = 0; i < FFbnd.cols(); i++) {
		/*
		 * MUSCL
		 */
		const double kappa = 1.0 / 3.0;
		const double eps = 1.0e-6;
		Eigen::Array3d dminus_L = Eigen::Array3d::Zero();
		if (i != 0) {
			dminus_L = Q.col(i) - Q.col(i - 1);
		}
		Eigen::Array3d dplus_L = Q.col(i + 1) - Q.col(i);
		Eigen::Array3d s_L;
		s_L = (2.0 * dplus_L * dminus_L + eps)
				/ (dplus_L * dplus_L + dminus_L * dminus_L + eps);
		Eigen::Vector3d mQ_L = Q.col(i);
		mQ_L.array() += s_L * 0.25
				* ((1.0 - kappa * s_L) * dminus_L
						+ (1.0 + kappa * s_L) * dplus_L);

		Eigen::Array3d dminus_R = Q.col(i + 1) - Q.col(i);
		Eigen::Array3d dplus_R = Eigen::Array3d::Zero();
		if (i != FFbnd.cols() - 1) {
			dplus_R = Q.col(i + 2) - Q.col(i + 1);
		}
		Eigen::Array3d s_R;
		s_R = (2.0 * dplus_R * dminus_R + eps)
				/ (dplus_R * dplus_R + dminus_R * dminus_R + eps);

		Eigen::Vector3d mQ_R = Q.col(i + 1);
		mQ_R.array() -= s_R * 0.25
				* ((1.0 - kappa * s_R) * dplus_R
						+ (1.0 + kappa * s_R) * dminus_R);

		/*
		 * Roe平均
		 */
		const double rho_L = mQ_L[quantity_d::Dencity];
		const double rho_R = mQ_R[quantity_d::Dencity];
//		const double rho = sqrt(rho_L * rho_R);

		const double u_L = mQ_L[quantity_d::Momentum] / rho_L;
		const double u_R = mQ_R[quantity_d::Momentum] / rho_R;
		const double u = (std::sqrt(rho_L) * u_L + std::sqrt(rho_R) * u_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		const double e_L = mQ_L[quantity_d::Energy];
		const double e_R = mQ_R[quantity_d::Energy];
		const double H_L = e_L / rho_L
				+ (Gamma - 1.0) * (e_L - 0.5 * rho_L * u_L * u_L) / rho_L;
		const double H_R = e_R / rho_R
				+ (Gamma - 1.0) * (e_R - 0.5 * rho_R * u_R * u_R) / rho_R;
		const double H = (std::sqrt(rho_L) * H_L + std::sqrt(rho_R) * H_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		// 音速
		const double a = std::sqrt((Gamma - 1.0) * (H - 0.5 * u * u));

		/*
		 * 流束ヤコビアン行列
		 */
		Eigen::Matrix3d R = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Rinv = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Lambda = Eigen::Matrix3d::Zero();

		Lambda(0, 0) = std::abs(u - a);
		Lambda(1, 1) = std::abs(u);
		Lambda(2, 2) = std::abs(u + a);

		R(0, 0) = 1.0;
		R(0, 1) = 1.0;
		R(0, 2) = 1.0;

		R(1, 0) = u - a;
		R(1, 1) = u;
		R(1, 2) = u + a;

		R(2, 0) = H - u * a;
		R(2, 1) = 0.5 * u * u;
		R(2, 2) = H + u * a;

		const double b1 = 0.5 * u * u * (Gamma - 1.0) / (a * a);
		const double b2 = (Gamma - 1.0) / (a * a);
		Rinv(0, 0) = 0.5 * (b1 + u / a);
		Rinv(0, 1) = -0.5 * (1.0 / a + b2 * u);
		Rinv(0, 2) = 0.5 * b2;

		Rinv(1, 0) = 1.0 - b1;
		Rinv(1, 1) = b2 * u;
		Rinv(1, 2) = -b2;

		Rinv(2, 0) = 0.5 * (b1 - u / a);
		Rinv(2, 1) = 0.5 * (1 / a - b2 * u);
		Rinv(2, 2) = 0.5 * b2;

		Eigen::Matrix3d A = R * Lambda * Rinv;

		FFbnd.col(i) = 0.5
				* (FF.col(i + 1) + FF.col(i) - A * (Q.col(i + 1) - Q.col(i)));
	}

	return FFbnd;
}

Eigen::Matrix3Xd muscl(const Eigen::Matrix3Xd &Q) {

	typedef cfd::quantity<double> quantity_d;

	/*
	 * 流束Flux
	 */
	Eigen::Matrix3Xd FF = Eigen::Matrix3Xd::Zero(quantity_d::Size, Q.cols());
	for (Eigen::Index i = 0; i < Q.cols(); i++) {
		const double rho = Q.col(i)[quantity_d::Dencity];
		const double u = Q.col(i)[quantity_d::Momentum]
				/ Q.col(i)[quantity_d::Dencity];
		const double e = Q.col(i)[quantity_d::Energy];
		const double p = (Gamma - 1.0) * (e - 0.5 * rho * u * u);

		FF.col(i)[quantity_d::Dencity] = Q.col(i)[quantity_d::Momentum];
		FF.col(i)[quantity_d::Momentum] = p + rho * u * u;
		FF.col(i)[quantity_d::Energy] = (e + p) * u;
	}

	// セル境界流束
	Eigen::Matrix3Xd FFbnd = Eigen::Matrix3Xd::Zero(quantity_d::Size,
			Q.cols() - 1);
	for (Eigen::Index i = 0; i < FFbnd.cols(); i++) {
		/*
		 * MUSCL
		 */
		const double kappa = 1.0 / 3.0;
		Eigen::Array3d dminus_L = Eigen::Array3d::Zero();
		if (i != 0) {
			dminus_L = Q.col(i) - Q.col(i - 1);
		}
		if (dminus_L[quantity_d::Dencity] < 0.0) {
			dminus_L = 0.0;
		}
		Eigen::Array3d dplus_L = Q.col(i + 1) - Q.col(i);
		Eigen::Vector3d mQ_L = Q.col(i);
		mQ_L.array() += 0.25
				* ((1.0 - kappa) * dminus_L + (1.0 + kappa) * dplus_L);

		Eigen::Array3d dminus_R = Q.col(i + 1) - Q.col(i);
		Eigen::Array3d dplus_R = Eigen::Array3d::Zero();
		if (i != FFbnd.cols() - 1) {
			dplus_R = Q.col(i + 2) - Q.col(i + 1);
		}
		Eigen::Vector3d mQ_R = Q.col(i + 1);
		mQ_R.array() -= 0.25
				* ((1.0 - kappa) * dplus_R + (1.0 + kappa) * dminus_R);

		/*
		 * Roe平均
		 */
		const double rho_L = mQ_L[quantity_d::Dencity];
		const double rho_R = mQ_R[quantity_d::Dencity];
//		const double rho = sqrt(rho_L * rho_R);

		const double u_L = mQ_L[quantity_d::Momentum] / rho_L;
		const double u_R = mQ_R[quantity_d::Momentum] / rho_R;
		const double u = (std::sqrt(rho_L) * u_L + std::sqrt(rho_R) * u_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		const double e_L = mQ_L[quantity_d::Energy];
		const double e_R = mQ_R[quantity_d::Energy];
		const double H_L = e_L / rho_L
				+ (Gamma - 1.0) * (e_L - 0.5 * rho_L * u_L * u_L) / rho_L;
		const double H_R = e_R / rho_R
				+ (Gamma - 1.0) * (e_R - 0.5 * rho_R * u_R * u_R) / rho_R;
		const double H = (std::sqrt(rho_L) * H_L + std::sqrt(rho_R) * H_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		// 音速
		const double a = std::sqrt((Gamma - 1.0) * (H - 0.5 * u * u));

		/*
		 * 流束ヤコビアン行列
		 */
		Eigen::Matrix3d R = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Rinv = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Lambda = Eigen::Matrix3d::Zero();

		Lambda(0, 0) = std::abs(u - a);
		Lambda(1, 1) = std::abs(u);
		Lambda(2, 2) = std::abs(u + a);

		R(0, 0) = 1.0;
		R(0, 1) = 1.0;
		R(0, 2) = 1.0;

		R(1, 0) = u - a;
		R(1, 1) = u;
		R(1, 2) = u + a;

		R(2, 0) = H - u * a;
		R(2, 1) = 0.5 * u * u;
		R(2, 2) = H + u * a;

		const double b1 = 0.5 * u * u * (Gamma - 1.0) / (a * a);
		const double b2 = (Gamma - 1.0) / (a * a);
		Rinv(0, 0) = 0.5 * (b1 + u / a);
		Rinv(0, 1) = -0.5 * (1.0 / a + b2 * u);
		Rinv(0, 2) = 0.5 * b2;

		Rinv(1, 0) = 1.0 - b1;
		Rinv(1, 1) = b2 * u;
		Rinv(1, 2) = -b2;

		Rinv(2, 0) = 0.5 * (b1 - u / a);
		Rinv(2, 1) = 0.5 * (1 / a - b2 * u);
		Rinv(2, 2) = 0.5 * b2;

		Eigen::Matrix3d A = R * Lambda * Rinv;

		FFbnd.col(i) = 0.5
				* (FF.col(i + 1) + FF.col(i) - A * (Q.col(i + 1) - Q.col(i)));
	}

	return FFbnd;
}

Eigen::Matrix3Xd roe(const Eigen::Matrix3Xd &Q) {

	typedef cfd::quantity<double> quantity_d;

	/*
	 * 流束Flux
	 */
	Eigen::Matrix3Xd FF = Eigen::Matrix3Xd::Zero(3, Q.cols());
	for (Eigen::Index i = 0; i < Q.cols(); i++) {
		const double rho = Q.col(i)[quantity_d::Dencity];
		const double u = Q.col(i)[quantity_d::Momentum]
				/ Q.col(i)[quantity_d::Dencity];
		const double e = Q.col(i)[quantity_d::Energy];
		const double p = (Gamma - 1.0) * (e - 0.5 * rho * u * u);

		FF.col(i)[quantity_d::Dencity] = Q.col(i)[quantity_d::Momentum];
		FF.col(i)[quantity_d::Momentum] = p + rho * u * u;
		FF.col(i)[quantity_d::Energy] = (e + p) * u;
	}

// セル境界流束
	Eigen::Matrix3Xd FFbnd = Eigen::Matrix3Xd::Zero(3, Q.cols() - 1);
	for (Eigen::Index i = 0; i < FFbnd.cols(); i++) {
		/*
		 * Roe平均
		 */
		const double rho_L = Q.col(i)[quantity_d::Dencity];
		const double rho_R = Q.col(i + 1)[quantity_d::Dencity];
//		const double rho = sqrt(rho_L * rho_R);

		const double u_L = Q.col(i)[quantity_d::Momentum]
				/ Q.col(i)[quantity_d::Dencity];
		const double u_R = Q.col(i + 1)[quantity_d::Momentum]
				/ Q.col(i + 1)[quantity_d::Dencity];
		const double u = (std::sqrt(rho_L) * u_L + std::sqrt(rho_R) * u_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		const double e_L = Q.col(i)[quantity_d::Energy];
		const double e_R = Q.col(i + 1)[quantity_d::Energy];
		const double H_L = e_L / rho_L
				+ (Gamma - 1.0) * (e_L - 0.5 * rho_L * u_L * u_L) / rho_L;
		const double H_R = e_R / rho_R
				+ (Gamma - 1.0) * (e_R - 0.5 * rho_R * u_R * u_R) / rho_R;
		const double H = (std::sqrt(rho_L) * H_L + std::sqrt(rho_R) * H_R)
				/ (std::sqrt(rho_L) + std::sqrt(rho_R));

		// 音速
		const double a = std::sqrt((Gamma - 1.0) * (H - 0.5 * u * u));

		/*
		 * 流束ヤコビアン行列
		 */
		Eigen::Matrix3d R = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Rinv = Eigen::Matrix3d::Zero();
		Eigen::Matrix3d Lambda = Eigen::Matrix3d::Zero();

		Lambda(0, 0) = std::abs(u - a);
		Lambda(1, 1) = std::abs(u);
		Lambda(2, 2) = std::abs(u + a);

		R(0, 0) = 1.0;
		R(0, 1) = 1.0;
		R(0, 2) = 1.0;

		R(1, 0) = u - a;
		R(1, 1) = u;
		R(1, 2) = u + a;

		R(2, 0) = H - u * a;
		R(2, 1) = 0.5 * u * u;
		R(2, 2) = H + u * a;

		const double b1 = 0.5 * u * u * (Gamma - 1.0) / (a * a);
		const double b2 = (Gamma - 1.0) / (a * a);
		Rinv(0, 0) = 0.5 * (b1 + u / a);
		Rinv(0, 1) = -0.5 * (1.0 / a + b2 * u);
		Rinv(0, 2) = 0.5 * b2;

		Rinv(1, 0) = 1.0 - b1;
		Rinv(1, 1) = b2 * u;
		Rinv(1, 2) = -b2;

		Rinv(2, 0) = 0.5 * (b1 - u / a);
		Rinv(2, 1) = 0.5 * (1 / a - b2 * u);
		Rinv(2, 2) = 0.5 * b2;

		Eigen::Matrix3d A = R * Lambda * Rinv;

		FFbnd.col(i) = 0.5
				* (FF.col(i + 1) + FF.col(i) - A * (Q.col(i + 1) - Q.col(i)));
	}

	return FFbnd;
}

void euler_time_integral(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F) {
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Q.col(i) - dt * (F.col(i) - F.col(i - 1)) / dx;
	}
	Q.col(0) = Q.col(1);
	Q.col(Q.cols() - 1) = Q.col(Q.cols() - 2);
}

void runge_kutta_2nd_roe(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F) {
	const double lambda1 = 1.0 / 3.0;
	const Eigen::Matrix3Xd Qn = Q;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda1 * dt * (F.col(i) - F.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F1 = roe(Q);
	const double lambda2 = 4.0 / 15.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda2 * dt * (F1.col(i) - F1.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F2 = roe(Q);
	const double lambda3 = 5.0 / 9.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda3 * dt * (F2.col(i) - F2.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F3 = roe(Q);
	const double lambda4 = 1.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda4 * dt * (F3.col(i) - F3.col(i - 1)) / dx;
	}
}


void runge_kutta_4th_roe(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F) {
	const double lambda1 = 1.0 / 4.0;
	const Eigen::Matrix3Xd Qn = Q;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda1 * dt * (F.col(i) - F.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F1 = roe(Q);
	const double lambda2 = 1.0 / 3.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda2 * dt * (F1.col(i) - F1.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F2 = roe(Q);
	const double lambda3 = 1.0 / 2.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda3 * dt * (F2.col(i) - F2.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F3 = roe(Q);
	const double lambda4 = 1.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda4 * dt * (F3.col(i) - F3.col(i - 1)) / dx;
	}
}

void runge_kutta_2nd_muscl(double dt, double dx, Eigen::Matrix3Xd &Q,
		const Eigen::Matrix3Xd &F) {
	const double lambda1 = 1.0 / 3.0;
	const Eigen::Matrix3Xd Qn = Q;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda1 * dt * (F.col(i) - F.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F1 = muscl(Q);
	const double lambda2 = 4.0 / 15.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda2 * dt * (F1.col(i) - F1.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F2 = muscl(Q);
	const double lambda3 = 5.0 / 9.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda3 * dt * (F2.col(i) - F2.col(i - 1)) / dx;
	}

	const Eigen::Matrix3Xd F3 = muscl(Q);
	const double lambda4 = 1.0;
	for (Eigen::Index i = 1; i < Q.cols() - 1; i++) {
		Q.col(i) = Qn.col(i) - lambda4 * dt * (F3.col(i) - F3.col(i - 1)) / dx;
	}
}
